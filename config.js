
var fs = require('fs');

// can have local file with ENV variables
var secretFile = './secret.json';
if (fs.existsSync(secretFile)) {
	var secret = require(secretFile);
	for(var p in secret){
		if( typeof(process.env[p]) === 'undefined' ){
			process.env[p] = secret[p];
		}
	}
}

module.exports = {
	USER: process.env.GIT_USER,
	TOKEN: process.env.GIT_TOKEN,
	WORKSPACE: process.env.WORKSPACE || 'workspace'
};


