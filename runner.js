
// ## dependencies
var exec = require('child_process');
var fs = require("fs");

var data = require('./lib/data');
var git = require('./lib/git');
var bash = require('./lib/bash');

var config = require('./config');

// ## config
var folder = config.WORKSPACE;

// ## export
var ns = {};
module.exports = ns;

// ## func
ns.extractAgrs = function(...args){
	var actual = args.filter(function(x){
		var exclude = (
			x.includes('node')
			|| x.includes('runner.js')
		);
		
		return !exclude;
	});
	
	var pipes = [];
	var params = {};
	
	return actual;
};

ns.eachPipe = function(p, force){
	var jobNames = p.jobs.map(x => x.name);
	console.log('runner.eachPipe >> p = %s | jobs = %s', p.name,  jobNames);
	
	var date = new Date();
	ts = date.toISOString();
	shortDate = ts.substring(0, 10).replace(/\-/g, '');
	
	for(var k=0; k<p.jobs.length; k++){
		var j = p.jobs[k];
		
		if( j.ref === 'git' ){
			
			var rc = git.clone(p.name, p.repo);
			if( !rc && !force ){
				console.log('runner.eachPipe - up to date...');
				return false;
			}
		}
		
		// TODO npm reference
		// TODO docker reference
		
		if( j.script && j.script.length ){
			var script_arr = j.script;
			
			if( typeof(j.script) === 'string' ){
				script_arr = [j.script];
			}
			
			for(var i=0; i<script_arr.length; i++){
				var targetScript = script_arr[i];
				if( typeof(targetScript) !== 'string' ){
					targetScript = targetScript.join(" \\\n");
				}
				targetScript = targetScript.replace('${DATE}', shortDate);
				console.log('runner.eachPipe - script.loop = %s', targetScript);
				
				bash.run(p.name, targetScript);
			}
			
		}
	}
};

ns.main = function(...args) {
	
	console.log('*** Lean-builder - start ***');
	var myArgs = ns.extractAgrs(...args);
	console.log('main >> args =', myArgs);
	console.log('main >> workspace =', folder);
	
	var pipes = data.getAllPipelines();
	var pipeNames = pipes.pipelines.map(x => x.name);
	console.log('main >> pipes =', pipeNames);
	var selectedPipes = pipes.pipelines;
	if( myArgs.length > 1){
		var argPipeNames = myArgs.filter(p => !p.includes('-'));
		console.log('main >> argPipeNames =', argPipeNames);
		selectedPipes = pipes.pipelines.filter(x => myArgs.includes(x.name));
	}
	
	if( selectedPipes.length === 0 ){
		console.log('main >> no selected pipelines');
		return false;
	}
	
	if( !fs.existsSync(folder) ){
		exec.execSync( 'mkdir ' + folder );
	}
	
	process.env.CI = 'true';
	process.env.NODE_ENV = "production";
	
	for(var i=0; i<selectedPipes.length; i++){
		var p = selectedPipes[i];
		var force = myArgs.includes('-f');
		ns.eachPipe(p, force);
		
	}
	
};

// ## flow
if( require.main === module ){
	ns.main(...process.argv);
}


