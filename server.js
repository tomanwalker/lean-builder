
// ## dep
var cron = require('node-cron');
var express = require('express');

var runner = require('./runner');
var db = require('./lib/data');

// ## config
var config = {
	PORT: Number(process.env.PORT || 3000),
	CHECK: Number(process.env.CHECK || 20)
};

var dict = {};
var pipeCounts = { db: 0, crons: 0 };

var app = express();

// ## export
var ns = {};
module.exports = ns;

// ## fund
ns.runPipeline = function(name){
	try{
		runner.main(name);
		var log = db.getRuns();
		
		log.rows.push({
			pipe: name,
			ts: ts
		});
		
		while( log.rows.length > 20 ){
			log.rows.shift();
		}
		
		db.saveRuns(log);
	}
	catch(err){
		console.log(err);
	}
};

ns.main = function(){
	
	var pipes = db.getAllPipelines();
	var result = pipes.pipelines.filter(x => x.schedules && x.schedules.length);
	
	var updateLen = pipes.pipelines.length;
	if( updateLen !== pipeCounts.db ){
		console.log('main - got - counts = %j', pipeCounts);
		pipeCounts.db = updateLen;
		pipeCounts.crons = result.length;
	}
	
	result.forEach(function(x){
		
		var key = [x.name, x.schedules[0].time].join('-');
		if( dict[key] ){
			return false;
		}
		
		console.log('main - scheduled - %s', key);
		dict[key] = cron.schedule(x.schedules[0].time, function(){
			
			var date = new Date();
			var ts = date.toISOString();
			console.log('cron.start - time = %s | name = %s', ts, x.name);
			ns.runPipeline(x.name);
			
		});
	});
	
	return 0;
};

// ## routes
app.get('/', function(req, res){
	res.json({ok: true});
});

app.get('/pipelines', function(req, res){
	
	var pipes = db.getAllPipelines();
	res.json(pipes);
});

app.get('/log', function(req, res){
	
	var log = db.getRuns();
	res.json(log);
});

app.post('/pipelines/:name', function(req, res){
	ns.runPipeline(req.params.name);
	res.json({ok: true});
});

// ## flow
console.log( '*** Start *** ');
console.log( 'flow - config = %j', config);
setTimeout(ns.main, 200);
setInterval(ns.main, (config.CHECK * 1000));

app.listen(config.PORT, function(){
	console.log('flow - server - started = %s', config.PORT);
});


