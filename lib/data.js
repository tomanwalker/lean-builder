
// ## dependencies
var fs = require("fs");

// ## config
var pipeFile = './pipelines.json';
var runLog = './data/run.json';

// ## export
var ns = {};
module.exports = ns;

/*
  * * * * * *
  | | | | | |
  | | | | | day of week
  | | | | month
  | | | day of month
  | | hour
  | minute
  second ( optional )
*/

// ## func
ns.getFile = function(fileName){
	var content = fs.readFileSync(fileName);
	var parsed = JSON.parse(content);
	
	return parsed;
};

ns.getAllPipelines = function(){
	
	var content = fs.readFileSync(pipeFile);
	var parsed = JSON.parse(content);
	
	return parsed;
};

ns.getRuns = function(){
	
	try{
		return ns.getFile(runLog);
	}
	catch(err){
		return {rows:[]};
	}
};

ns.saveRuns = function(data){
	
	var txt = JSON.stringify(data, null, 2);
	fs.writeFileSync(runLog, txt);
	
};


