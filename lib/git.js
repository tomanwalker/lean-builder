
// ## dependencies
var fs = require("fs");
var exec = require('child_process');

var config = require('../config');

// ## config
var folder = config.WORKSPACE;

// ## export
var ns = {};
module.exports = ns;

// ## func
ns.compare = function(target){
	
	console.log('---compare-s---');
	var res = exec.execSync( 'cd ' + target + ' && git remote update && git status' );
	res = res.toString();
	console.log( res );
	console.log('---compare-e---');
	if( res.includes('Your branch is behind') ){
		return 1;
	}
	
	return 0;
	
};
ns.clone = function(name, repo){
	
	console.log('git.clone >> start - project =', name);
	
	var target = folder + '/' + name;
	var cmd = ('cd ' + folder + ' && git clone ' + repo);
	if( fs.existsSync(target) ){
		
		var res = ns.compare(target);
		if( res === 0 ){
			return false;
		}
		
		cmd = ('cd ' + target + ' && git pull');
	}
	
	console.log('git.clone >> cmd = %s', cmd);
	exec.execSync( cmd, {stdio: 'inherit'} );
	
	console.log('git.clone >> done...');
	return true;
	
};


