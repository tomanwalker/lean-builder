
// ## dependencies
var exec = require('child_process');

var config = require('../config');

// ## config
var folder = config.WORKSPACE;

// ## export
var ns = {};
module.exports = ns;

// ## func
ns.run = function(name, cmd){
	console.log('bash.run >> start - project =', name);
	var torun = 'cd ' + folder + '/' + name + ' && ' + cmd;
	
	console.log('bash.run >> cmd =', torun);
	exec.execSync(torun, {stdio: 'inherit'});
	
	console.log('bash.run >> done...');
	return true;
};


